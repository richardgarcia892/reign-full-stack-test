import { Document } from 'mongoose';

export interface Article extends Document {
  id?: number,
  discarted: boolean,
  created_at: string,
  title: string,
  url: string,
  created_at_i: number,
  author: string
}
