import { Schema } from 'mongoose';

export const ArticleSchema = new Schema(
  {
    "discarted": { type: Boolean, default: false },
    "created_at": String,
    "title": String,
    "url": String,
    "created_at_i": Number,
    "author": String

  })