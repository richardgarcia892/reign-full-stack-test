import { Controller, Get, Param, Put } from '@nestjs/common';

import { ArticlesService } from './articles.service';
import { Article } from './interface/articles.interface';

@Controller('articles')
export class ArticlesController {

  constructor(private articleService: ArticlesService) {}

  @Get()
  getArticles(): Promise<Article[]> {
    return this.articleService.getArticles()
  }

  @Put('discard/:articleId')
  articleDiscard(@Param('articleId') articleId): Promise<Article> {
    return this.articleService.discardArticle(articleId);
  }
}
