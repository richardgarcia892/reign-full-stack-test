import { HttpService, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Article } from './interface/articles.interface';


@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel('Article') private articleModel: Model<Article>,
    private httpService: HttpService
  ) {}


  async getArticles(): Promise<Article[]> {
    return await this.articleModel.find({ discarted: false }).sort({ created_at_i: -1 })
  }
  /*async getArticles(): Promise<Article[]> {
    return await this.articleModel.findById('5ffabe904588bc2950443938')
  }*/

  async discardArticle(id): Promise<Article> {
    return await this.articleModel.findByIdAndUpdate(id, { discarted: true }, { new: true })
  }

  // Pull news from API when software starts
  onModuleInit() {
    this.feedArticles();
  }

  // Chrome job to pull hits from the API every hour
  @Cron('0 * * * *')
  async feedArticles() {
    const rawArticlesList = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    const articleArray = rawArticlesList.data.hits
    // ensure that both tittle and story_tittle are not null, otherwise, ignore.
    articleArray.forEach(article => {
      if (article.title !== null || article.story_title !== null) {
        // Save remeaning filtered articles to mongoDB.
        const newArticle = new this.articleModel({
          title: article.title ?? article.story_title,
          url: article.url ?? article.story_url,
          created_at: article.created_at,
          created_at_i: article.created_at_i,
          author: article.author
        })
        newArticle.save().catch(err => {
          console.error(err)
        })
      }
    })
  }
}
