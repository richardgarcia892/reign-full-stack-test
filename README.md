# Running the application

As was said, a docker compose file has being set up to be able to run the application asap,
once docker compose is done, you may reach the front end with the following link:  http://localhost:8888/.

# General

The complete project structure is the following:
as it was said, the application only uses NestJs and MongoDB for the backend, no any more modules nor package where used.

The FrontEnd uses only AngularJs (latest) alongside css and HTML, no boostrap, material or any other extension / framework where used.
```bash
├── README.md
├── client
│   ├── README.md
│   ├── angular.json
│   ├── dist
│   │   └── client
│   ├── dockerfile
│   ├── e2e
│   │   ├── protractor.conf.js
│   │   ├── src
│   │   └── tsconfig.json
│   ├── karma.conf.js
│   ├── nginx.conf
│   ├── package-lock.json
│   ├── package.json
│   ├── src
│   │   ├── app
│   │   ├── assets
│   │   ├── environments
│   │   ├── favicon.ico
│   │   ├── index.html
│   │   ├── main.ts
│   │   ├── polyfills.ts
│   │   ├── styles.css
│   │   └── test.ts
│   ├── tsconfig.app.json
│   ├── tsconfig.json
│   ├── tsconfig.spec.json
│   └── tslint.json
├── docker-compose.yml
└── server
    ├── README.md
    ├── dist
    │   ├── app.controller.d.ts
    │   ├── app.controller.js
    │   ├── app.controller.js.map
    │   ├── app.module.d.ts
    │   ├── app.module.js
    │   ├── app.module.js.map
    │   ├── app.service.d.ts
    │   ├── app.service.js
    │   ├── app.service.js.map
    │   ├── articles
    │   ├── main.d.ts
    │   ├── main.js
    │   ├── main.js.map
    │   └── tsconfig.build.tsbuildinfo
    ├── dockerfile
    ├── nest-cli.json
    ├── package-lock.json
    ├── package.json
    ├── src
    │   ├── app.controller.spec.ts
    │   ├── app.controller.ts
    │   ├── app.module.ts
    │   ├── app.service.ts
    │   ├── articles
    │   └── main.ts
    ├── test
    │   ├── app.e2e-spec.ts
    │   └── jest-e2e.json
    ├── tsconfig.build.json
    └── tsconfig.json
```

# Server

The application application has four main features:

 1. Once the application is started, makes call to the given HN Api and fill the databases with currently retrieves news. This process can be found in `server\src\articles\articles.service.ts` using `onModuleInit()` to call  `feedArticles()` method.
 2. The `feedArticles()`, is also called from a cron job whit the following configuration: `@Cron('0 * * * *')` ensuring to run only once per hour (at minute 0, every hour).
 3. the `feedArticles()` does not only call the HN API and fill the database, it does also ensure that the retrevies nows has neither title or story_title, and feels the database only with valid news (as the document said, news that has null title nor history_title must be ignored).
 4. to be able to properly discard news in the front end using the API, a schema parameter called "discarted" with type Boolean was implemented, once an user hits the trash button in the front end, the API updates this attribute in the database as TRUE, so each time an user access the site or refresh the front end "discarted" news wont show up.  
 
 ## backend API  
The backend API runs in port 3000 by default and has only two URL:
GET: http://localhost:3000/articles
this endpoint retrieve an array with all articles currently stores in the database, sort from newest to oldest.

PUT: http://localhost:8888/articles/discard/:_id
using the mongo id as identifier this method set the given articles as discarted = true in the database.
# Client

The client application has 2 main modules: Article and Articles.

 - In the Articles Module modules, everything related to the articles Array is handled, this module has the following key feature:
 - Use `ngOnInit()` to call `getArticles()` Service method to fill the articles array as soon as the user visit the site
 - Trigger `discardArticle()` to discard an article and by both, executing the backend API to set the article as discarted and taking it out from the array to update que UI.

In the other hand, the ArticleModule, has the ArticleService where all the key methods are coded, the Article folder is also where Article Interface is located.


 
# General Diagram

This is a General diagram about how the application works. Note that I have little experience doing sequence diagram so there might by some mistakes in this one.

```mermaid
sequenceDiagram
User ->> Client: access the client
Client ->> Server: getArticles( )
Server -->> Client: articles[ ]
User ->> Client: Click Trash in any article
Client -->> Server: discardArticle(article._id)
Server-->>Database: discardArticle( )
Client -->> User: articles[]

```

