import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Article } from './article'

const httpOptions = {
  headers: new HttpHeaders({})
}

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  articleUrl: string = 'http://localhost:3000/articles'

  constructor(private http: HttpClient) {}

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(`${this.articleUrl}`);
  }

  discardArticle(article: Article): void {
    const url: string = `${this.articleUrl}/discard/${article._id}`
    this.http.put<Article>(url, httpOptions).subscribe(res => {});
  }
}
