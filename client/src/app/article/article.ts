export interface Article {
  _id: any;
  id?: number
  discarted?: boolean,
  created_at: string | null,
  created_at_i: number | null,
  title: string | null,
  url: string | null,
  author: string | null,
}
