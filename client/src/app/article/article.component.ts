import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ArticleService } from './article.service'

import { Article } from './article'

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @Input() article!: Article;
  @Output() discardArticle: EventEmitter<Article> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onDiscard(article: Article) {
    this.discardArticle.emit(article)
  }
}
